"use strict";

const { genSaltSync, hashSync } = require("bcryptjs");
const fs = require("fs");

module.exports = {
  up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    const users = JSON.parse(fs.readFileSync("./data/users.json", "utf-8")).map(
      (u) => {
        const salt = genSaltSync();
        const password = hashSync(u.password, salt);

        return {
          ...u,
          password,
          createdAt: new Date(),
          updatedAt: new Date(),
        };
      }
    );

    return queryInterface.bulkInsert("Users", users);
  },

  down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete("Users");
  },
};
