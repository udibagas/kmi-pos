const { verify, decode } = require("jsonwebtoken");
const UserService = require("../modules/users/UserService");

const verifyToken = async (req, res, next) => {
  const bearerHeader = req.headers.authorization;

  if (bearerHeader === undefined) {
    return res.status(403).json({ message: "Token is required" });
  }

  const token = bearerHeader.split(" ")[1];
  if (!token) return res.status(403).json({ message: "Token is required" });

  try {
    const decoded = verify(token, process.env.TOKEN_KEY);
    req.user = await UserService.getOneByEmail(decoded.email)
  } catch (error) {
    return res.status(401).json({ message: "Invalid token" });
  }

  return next();
};

module.exports = verifyToken;
