const { DB_USER, DB_PASS, DB_NAME, DB_HOST, DB_DIALECT } = process.env;

module.exports = {
  development: {
    username: DB_USER || "root",
    password: DB_PASS || "bismillah",
    database: DB_NAME || "kmi_pos",
    host: DB_HOST || "127.0.0.1",
    dialect: DB_DIALECT || "mysql",
  },
  test: {
    username: "root",
    password: null,
    database: "database_test",
    host: "127.0.0.1",
    dialect: "mysql",
  },
  production: {
    username: "root",
    password: null,
    database: "database_production",
    host: "127.0.0.1",
    dialect: "mysql",
  },
};
