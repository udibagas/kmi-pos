"use strict";

const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);
const router = require("express").Router();

router.get("/", function (req, res) {
  res.send("OK");
});

router.use(require("./auth"));
router.use(require("../middlewares/AuthMiddleware"));

// baca di routes
fs.readdirSync(__dirname)
  .filter((file) => {
    const excluded = [basename, "auth.js"];
    return (
      file.indexOf(".") !== 0 &&
      file.slice(-3) === ".js" &&
      !excluded.includes(file)
    );
  })
  .forEach((file) => {
    router.use(`/${file.split(".")[0]}`, require(`./${file}`));
  });

// baca di modules
fs.readdirSync(`${__dirname}/../modules`).forEach((dir) => {
  const controllerFile = fs
    .readdirSync(`${__dirname}/../modules/${dir}`)
    .find((file) => file.toLowerCase().includes("controller.js"));

  if (controllerFile) {
    const controller = require(`${__dirname}/../modules/${dir}/${controllerFile}`);
    const instance = new controller();

    for (let fn of instance.routes) {
      router.use(instance[fn](`/${dir}`));
    }
  }
});

module.exports = router;
