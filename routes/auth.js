const AuthController = require("../controllers/AuthController");
const router = require("express").Router();
const schema = require("../validations/LoginSchema");
const validate = require("../middlewares/ValidatorMiddleware");

router.post("/login", validate(schema), AuthController.login);

module.exports = router;
