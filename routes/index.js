const router = require("express").Router();
// const { generatePdf } = require("html-pdf-node");
const purchaseService = require("../modules/purchases/PurchaseService");

router.get("/purchases/print/:id", async (req, res) => {
  try {
    const po = await purchaseService.getOneById(req.params.id);
    res.render("po/print", { po });
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
