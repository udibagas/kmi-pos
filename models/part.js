"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Part extends Model {
    static associate(models) {
      Part.belongsTo(models.Category);

      Part.belongsTo(models.Vendor);

      Part.hasMany(models.PurchaseItem, {
        foreignKey: "purchasableId",
        constraints: false,
        scope: {
          purchasableType: "part",
        },
      });
    }
  }

  Part.init(
    {
      partNumber: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
      },
      hsCode: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      interchangable: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      countryOfOrigin: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      duty: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      freight: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      stock: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Part Number is required",
          },
          notEmpty: {
            msg: "Part Number is required",
          },
          isInt: {
            msg: "Stock must be a number",
          },
        },
      },
      price: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Price required",
          },
          notEmpty: {
            msg: "Price required",
          },
          isInt: {
            msg: "Price must be a number",
          },
        },
      },
      CategoryId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Category is required",
          },
          notEmpty: {
            msg: "Category is required",
          },
        },
      },
      VendorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      paranoid: true,
      sequelize,
      modelName: "Part",
    }
  );

  return Part;
};
