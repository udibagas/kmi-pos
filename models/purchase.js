"use strict";
const { Model } = require("sequelize");
var { toWords } = require("number-to-words");

module.exports = (sequelize, DataTypes) => {
  class Purchase extends Model {
    static associate(models) {
      Purchase.hasMany(models.PurchaseItem, { as: "items" });
      Purchase.hasMany(models.PurchaseApproval);
      Purchase.belongsTo(models.Vendor);
      Purchase.belongsTo(models.User);
      Purchase.belongsTo(models.Store);
    }

    async generateNumber() {
      const lastPurchase = await Purchase.findOne({
        orderBy: [["createdAt", "desc"]],
      });

      if (!lastPurchase) return "KMI0000001";

      let number = Number(lastPurchase.number.replace("KMI", "")) + 1;
      return "KMI" + number.toString().padStart(7, "0");
    }

    get formattedDate() {
      return this.date
        ? this.date.toLocaleString("id-ID", { dateStyle: "medium" })
        : "";
    }
  }

  Purchase.init(
    {
      date: {
        type: DataTypes.DATE,
        // get() {
        //   const rawValue = this.getDataValue("date");
        //   return rawValue
        //     ? rawValue.toLocaleString("id-ID", { dateStyle: "medium" })
        //     : "";
        // },
        // allowNull: false,
        // validate: {
        //   notNull: {
        //     msg: "Date is required",
        //   },
        //   notEmpty: {
        //     msg: "Date is required",
        //   },
        // },
      },
      number: {
        type: DataTypes.STRING,
        // allowNull: false,
        // unique: true,
      },
      UserId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "User is required",
          },
          notEmpty: {
            msg: "User is required",
          },
        },
      },
      totalAmount: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
      VendorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Vendor is required",
          },
          notEmpty: {
            msg: "Vendor is required",
          },
        },
      },
      vendorAddress: {
        type: DataTypes.TEXT,
      },
      vendorPhone: {
        type: DataTypes.STRING,
      },
      vendorFax: {
        type: DataTypes.STRING,
      },
      vendorAddress: {
        type: DataTypes.STRING,
      },
      status: DataTypes.BOOLEAN,
      statusLabel: {
        type: DataTypes.VIRTUAL,
        get() {
          if (this.status === null) return "DRAFT";
          return this.status ? "APPROVED" : "REJECTED";
        },
      },
      type: {
        type: DataTypes.STRING,
        // allowNull: false,
        // validate: {
        //   notNull: {
        //     msg: "Type is required",
        //   },
        //   notEmpty: {
        //     msg: "Type is required",
        //   },
        //   isIn: [["part", "machine"]],
        // },
      },
      toc: {
        type: DataTypes.TEXT,
      },
      StoreId: {
        type: DataTypes.INTEGER,
        // allowNull: false,
      },
      vendorAttention: {
        type: DataTypes.STRING,
      },
      destination: {
        type: DataTypes.STRING,
      },
      shippingMode: {
        type: DataTypes.STRING,
      },
      deliveryCondition: {
        type: DataTypes.STRING,
      },
      comment: {
        type: DataTypes.TEXT,
      },
      orderType: {
        type: DataTypes.STRING,
      },
      partialShipment: {
        type: DataTypes.STRING,
      },
      paymentType: {
        type: DataTypes.STRING,
      },
      vendorTerm: {
        type: DataTypes.STRING,
      },
      packingCondition: {
        type: DataTypes.STRING,
      },
      customerOrderNumber: {
        type: DataTypes.STRING,
      },
      currency: {
        type: DataTypes.STRING,
      },
      email: {
        type: DataTypes.STRING,
      },
      subTotal: {
        type: DataTypes.BIGINT,
      },
      shippingCost: {
        type: DataTypes.BIGINT,
      },
      tax: {
        type: DataTypes.BIGINT,
      },
      discount: {
        type: DataTypes.BIGINT,
      },
      totalAmountBalance: {
        type: DataTypes.BIGINT,
      },
      totalAmountBalanceSaid: {
        type: DataTypes.TEXT,
      },
      note: {
        type: DataTypes.TEXT,
      },
    },
    {
      paranoid: true,
      sequelize,
      modelName: "Purchase",
    }
  );

  Purchase.beforeCreate(async (purchase, options) => {
    purchase.date = new Date();
    purchase.number = await purchase.generateNumber();
  });

  // Purchase.beforeSave((purchase, options) => {
  //   if (!isNaN(purchase.totalAmountBalance)) {
  //     purchase.totalAmountBalanceSaid = toWords(purchase.totalAmountBalance);
  //   }
  // });

  return Purchase;
};
