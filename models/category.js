"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Category extends Model {
    static associate(models) {
      Category.hasMany(models.Machine);
      Category.hasMany(models.Part);
    }
  }

  Category.init(
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notNull: {
            msg: "Code is required",
          },
          notEmpty: {
            msg: "Code is required",
          },
          len: {
            args: [1, 255],
            msg: "Code is max 255 characters",
          },
        },
      },
      name: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
          len: {
            args: [1, 255],
            msg: "Name is max 255 characters",
          },
        },
      },
      description: DataTypes.TEXT,
    },
    {
      paranoid: true,
      sequelize,
      modelName: "Category",
    }
  );

  return Category;
};
