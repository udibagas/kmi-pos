"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Vendor extends Model {
    static associate(models) {
      Vendor.hasMany(models.Purchase);
    }
  }
  Vendor.init(
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notNull: {
            msg: "Code is required",
          },
          notEmpty: {
            msg: "Code is required",
          },
        },
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
        },
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      phone: DataTypes.STRING,
      fax: DataTypes.STRING,
      email: DataTypes.STRING,
      contactPerson: DataTypes.STRING,
    },
    {
      paranoid: true,
      sequelize,
      modelName: "Vendor",
    }
  );

  return Vendor;
};
