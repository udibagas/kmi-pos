"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class PurchaseApproval extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      PurchaseApproval.belongsTo(models.Purchase);
      PurchaseApproval.belongsTo(models.User);
    }
  }

  PurchaseApproval.init(
    {
      attempt: {
        type: DataTypes.INTEGER,
        // allowNull: false,
      },
      PurchaseId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      UserId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      status: DataTypes.BOOLEAN,
      note: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
    },
    {
      paranoid: true,
      sequelize,
      modelName: "PurchaseApproval",
    }
  );

  PurchaseApproval.beforeCreate(async (approval, options) => {
    const lastAttempt = await PurchaseApproval.count({
      where: { PurchaseId: approval.PurchaseId },
    });
    approval.attempt = lastAttempt + 1;
  });

  PurchaseApproval.afterCreate(async (approval, options) => {
    await sequelize.models.Purchase.update(
      { status: approval.status },
      { where: { id: approval.PurchaseId } }
    );
  });

  return PurchaseApproval;
};
