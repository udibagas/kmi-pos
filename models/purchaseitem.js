"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PurchaseItem extends Model {
    static associate(models) {
      PurchaseItem.belongsTo(models.Purchase);

      PurchaseItem.belongsTo(models.Part, {
        foreignKey: "purchasableId",
        constraints: false,
      });

      PurchaseItem.belongsTo(models.Machine, {
        foreignKey: "purchasableId",
        constraints: false,
      });
    }
  }

  PurchaseItem.init(
    {
      PurchaseId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Purchase Id is required",
          },
          notEmpty: {
            msg: "Purchase Id is required",
          },
        },
      },
      purchasableId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Item is required",
          },
          notEmpty: {
            msg: "Item is required",
          },
        },
      },
      purchasableType: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Item type is required",
          },
          notEmpty: {
            msg: "Item type is required",
          },
          isIn: [["part", "machine"]],
        },
      },
      qty: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Qty is required",
          },
          notEmpty: {
            msg: "Qty is required",
          },
          isInt: {
            msg: "Qty must be a number",
          },
        },
      },
      unitPrice: {
        type: DataTypes.BIGINT,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Unit Price is required",
          },
          notEmpty: {
            msg: "Unit Price is required",
          },
          isInt: {
            msg: "Unit Price must be a number",
          },
        },
      },
      amount: {
        type: DataTypes.BIGINT,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Amount is required",
          },
          notEmpty: {
            msg: "Amount is required",
          },
          isInt: {
            msg: "Amount must be a number",
          },
        },
      },
      description: DataTypes.TEXT,
    },
    {
      paranoid: true,
      sequelize,
      modelName: "PurchaseItem",
    }
  );

  PurchaseItem.beforeBulkCreate((item) => {
    item.amount = item.qty * item.unitPrice;
  });

  return PurchaseItem;
};
