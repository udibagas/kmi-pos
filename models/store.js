"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Store extends Model {
    static associate(models) {
      Store.hasMany(models.Purchase);
      Store.hasMany(models.Sale);
    }
  }
  Store.init(
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notNull: {
            msg: "Code is required",
          },
          notEmpty: {
            msg: "Code is required",
          },
          len: {
            args: [1, 255],
            msg: "Max code is 255 characters",
          },
        },
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
          len: {
            args: [1, 255],
            msg: "Max name is 255 characters",
          },
        },
      },
      description: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [0, 255],
            msg: "Max description is 255 characters",
          },
        },
      },
    },
    {
      paranoid: true,
      sequelize,
      modelName: "Store",
    }
  );

  return Store;
};
