"use strict";
const { Model } = require("sequelize");
const { genSaltSync, hashSync } = require("bcryptjs");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  User.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          notEmpty: {
            msg: "Password is required",
          },
          min: {
            args: 8,
            msg: "Minium password is 8 characters",
          },
        },
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          notNull: {
            msg: "Email is required",
          },
          notEmpty: {
            msg: "Email is required",
          },
          isEmail: {
            msg: "Email is not valid",
          },
        },
      },
      status: DataTypes.BOOLEAN,
      role: DataTypes.BOOLEAN,
    },
    {
      paranoid: true,
      sequelize,
      modelName: "User",
      defaultScope: {
        attributes: { exclude: ["password", "deletedAt"] },
      },
    }
  );

  User.addScope("withPassword", {
    attributes: {},
  });

  User.addScope("active", {
    where: { status: true },
  });

  User.beforeSave((user, options) => {
    user.email = user.email.toLowerCase();
    if (user.password) {
      const salt = genSaltSync();
      user.password = hashSync(user.password, salt);
    }
  });

  User.afterSave((user, options) => {
    delete user.dataValues.password;
  });

  return User;
};
