"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Sale extends Model {
    static associate(models) {
      Sale.hasMany(models.SaleItem);
    }
  }
  Sale.init(
    {
      date: DataTypes.DATE,
      number: DataTypes.STRING,
      UserId: DataTypes.INTEGER,
      totalAmount: DataTypes.BIGINT,
      CustomerId: DataTypes.INTEGER,
      status: DataTypes.BOOLEAN,
      type: DataTypes.STRING,
      StoreId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Sale",
    }
  );
  return Sale;
};
