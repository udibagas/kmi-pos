"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    static associate(models) {
      // define association here
    }
  }
  Customer.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
          len: {
            args: [1, 255],
            msg: "Name is max 255 characters",
          },
        },
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
        },
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: {
            args: [1, 255],
            msg: "Phone is max 255 characters",
          },
        },
      },
      email: {
        type: DataTypes.STRING,
        validate: {
          isEmail: {
            msg: "Email must be valid email address",
          },
          len: {
            args: [1, 255],
            msg: "Email is max 255 characters",
          },
        },
      },
      contactPerson: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [1, 255],
            msg: "Contact person is max 255 characters",
          },
        },
      },
    },
    {
      paranoid: true,
      sequelize,
      modelName: "Customer",
    }
  );

  return Customer;
};
