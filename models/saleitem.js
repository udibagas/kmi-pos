"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SaleItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      SaleItem.belongsTo(models.Sale);

      SaleItem.belongsTo(models.Part, {
        foreignKey: "saleableId",
        constraints: false,
      });

      SaleItem.belongsTo(models.Machine, {
        foreignKey: "saleableId",
        constraints: false,
      });
    }
  }
  SaleItem.init(
    {
      SaleId: DataTypes.INTEGER,
      saleableId: DataTypes.INTEGER,
      saleableType: DataTypes.STRING,
      qty: DataTypes.INTEGER,
      amount: DataTypes.BIGINT,
      description: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "SaleItem",
    }
  );
  return SaleItem;
};
