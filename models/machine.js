"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Machine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Machine.belongsTo(models.Category);

      Machine.belongsTo(models.Vendor);

      Machine.hasMany(models.PurchaseItem, {
        foreignKey: "purchasableId",
        constraints: false,
        scope: {
          purchasableType: "machine",
        },
      });
    }
  }
  Machine.init(
    {
      machineModel: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      machineCode: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      machineSerialNumber: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      operatingWeight: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      bucketCapacity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      engineModel: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      engineRatedPower: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      engineDisplacement: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      engineSerialNumber: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      stock: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      price: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      CategoryId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      VendorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Machine",
    }
  );
  return Machine;
};
