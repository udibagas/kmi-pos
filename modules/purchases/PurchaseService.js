const { Purchase, sequelize } = require("../../models");
const ApiService = require("../../services/ApiService");

class PurchaseService extends ApiService {
  constructor() {
    super(Purchase);
  }

  async create(data) {
    try {
      const purchase = sequelize.transaction(async (transaction) => {
        data.items = data.items.map((i) => {
          i.purchasableType = data.type;
          return i;
        });

        return Purchase.create(data, {
          include: "items",
          transaction,
        });
      });

      return purchase;
    } catch (error) {
      const { name, message } = error;

      if (["SequelizeValidationError", "ValidationError"].includes(name)) {
        const errors = this.parseError(error);
        throw { status: 400, name: "ValidationError", errors };
      }

      throw { status: 500, name, message };
    }
  }

  async updateById(data, id) {
    try {
      const purchase = await this.getOneById(id);
      const result = sequelize.transaction(async (transaction) => {
        data.items = data.items.map((i) => {
          i.purchasableType = data.type;
          return i;
        });

        return purchase.update(data, {
          include: "items",
          transaction,
        });
      });

      return result;
    } catch (error) {
      const { name, message } = error;

      if (["SequelizeValidationError", "ValidationError"].includes(name)) {
        const errors = this.parseError(error);
        throw { status: 400, name: "ValidationError", errors };
      }

      throw { status: 500, name, message };
    }
  }

  async approve(id, data) {
    try {
      const purchase = await this.getOneById(id);
      const approval = await purchase.createPurchaseApproval(data);
      return approval;
    } catch (error) {
      const { name, message } = error;

      if (["SequelizeValidationError", "ValidationError"].includes(name)) {
        const errors = this.parseError(error);
        throw { status: 400, name: "ValidationError", errors };
      }

      throw { status: 500, name, message };
    }
  }
}

module.exports = new PurchaseService();
