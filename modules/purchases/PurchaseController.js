const { ApiController, router } = require("../../controllers/ApiController");
const PurchaseService = require("./PurchaseService");
const PurchaseSchema = require("./PurchaseSchema");

class PurchaseController extends ApiController {
  searchable = ["number", "customerOrderNumber"];

  filterable = ["VendorId", "StoreId"];

  defaultOrder = "date";

  defaultSort = "desc";

  include = ["Vendor", "Store", "User", "items"];

  routes = ["index", "show", "create", "update", "destroy", "approve"];

  constructor() {
    super(PurchaseService, PurchaseSchema);
  }

  approve(path) {
    return router.post(`${path}/approve/:id`, async (req, res) => {
      try {
        const { note, status } = req.body;
        const approval = await this.service.approve(req.params.id, {
          note,
          status,
          UserId: req.user.id,
        });
        res.send({ message: "Approval has been saved", approval });
      } catch (error) {
        res.status(error.status || 500).json(error);
      }
    });
  }
}

module.exports = PurchaseController;
