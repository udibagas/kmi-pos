const { Sale } = require("../../models");
const ApiService = require("../../services/ApiService");

class SaleService extends ApiService {
  constructor() {
    super(Sale);
  }
}

module.exports = new SaleService();
