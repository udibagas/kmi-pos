const { ApiController } = require("../../controllers/ApiController");
const SaleService = require("./SaleService");
const SaleSchema = require("./SaleSchema");

class SaleController extends ApiController {
  searchable = ["code", "name", "description"];

  defaultOrder = "name";

  defaultSort = "asc";

  constructor() {
    super(SaleService, SaleSchema);
  }
}

module.exports = SaleController;
