const { Customer } = require("../../models");
const ApiService = require("../../services/ApiService");

class CustomerService extends ApiService {
  constructor() {
    super(Customer);
  }
}

module.exports = new CustomerService();
