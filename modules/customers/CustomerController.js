const { ApiController } = require("../../controllers/ApiController");
const CustomerService = require("./CustomerService");
const CustomerSchema = require("./CustomerSchema");

class CustomerController extends ApiController {
  searchable = ["name"];

  defaultOrder = "name";

  defaultSort = "asc";

  constructor() {
    super(CustomerService, CustomerSchema);
  }
}

module.exports = CustomerController;
