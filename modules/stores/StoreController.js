const { ApiController } = require("../../controllers/ApiController");
const StoreService = require("./StoreService");
const StoreSchema = require("./StoreSchema");

class StoreController extends ApiController {
  searchable = ["code", "name", "description"];

  defaultOrder = "name";

  defaultSort = "asc";

  constructor() {
    super(StoreService, StoreSchema);
  }
}

module.exports = StoreController;
