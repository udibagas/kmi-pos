const { Store } = require("../../models");
const ApiService = require("../../services/ApiService");

class StoreService extends ApiService {
  constructor() {
    super(Store);
  }
}

module.exports = new StoreService();
