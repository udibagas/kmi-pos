const { ApiController } = require("../../controllers/ApiController");
const MachineService = require("./MachineService");
const MachineSchema = require("./MachineSchema");

class MachineController extends ApiController {
  searchable = [
    "machineCode",
    "machineSerialNumber",
    "machineModel",
    "engineModel",
    "engineSerialNumber",
  ];

  filterable = ["VendorId", "CategoryId"];

  defaultOrder = "machineModel";

  defaultSort = "asc";

  include = ["Category", "Vendor"];

  constructor() {
    super(MachineService, MachineSchema);
  }
}

module.exports = MachineController;
