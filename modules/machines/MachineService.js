const { Machine } = require("../../models");
const ApiService = require("../../services/ApiService");

class MachineService extends ApiService {
  constructor() {
    super(Machine);
  }
}

module.exports = new MachineService();
