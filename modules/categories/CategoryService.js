const { Category } = require("../../models");
const ApiService = require("../../services/ApiService");

class CategoryService extends ApiService {
  constructor() {
    super(Category);
  }
}

module.exports = new CategoryService();
