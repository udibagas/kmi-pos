module.exports = {
  code: { type: "string" },
  name: { type: "string" },
  description: { type: "string", optional: true },
};
