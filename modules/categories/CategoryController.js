const { ApiController } = require("../../controllers/ApiController");
const CategoryService = require("./CategoryService");
const CategorySchema = require("./CategorySchema");

class CategoryController extends ApiController {
  searchable = ["code", "name", "description"];

  defaultOrder = "name";

  defaultSort = "asc";

  constructor() {
    super(CategoryService, CategorySchema);
  }
}

module.exports = CategoryController;
