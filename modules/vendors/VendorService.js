const { Vendor } = require("../../models");
const ApiService = require("../../services/ApiService");

class VendorService extends ApiService {
  constructor() {
    super(Vendor);
  }
}

module.exports = new VendorService();
