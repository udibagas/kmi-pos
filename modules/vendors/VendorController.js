const { ApiController } = require("../../controllers/ApiController");
const VendorService = require("./VendorService");
const VendorSchema = require("./VendorSchema");

class VendorController extends ApiController {
  searchable = ["code", "name", "description"];

  defaultOrder = "name";

  defaultSort = "asc";

  constructor() {
    super(VendorService, VendorSchema);
  }
}

module.exports = VendorController;
