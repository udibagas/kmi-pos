module.exports = {
  code: { type: "string" },
  name: { type: "string" },
  address: { type: "string" },
  phone: { type: "string" },
  email: { type: "string" },
  contactPerson: { type: "string" },
};
