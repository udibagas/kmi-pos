const { ApiController } = require("../../controllers/ApiController");
const PartService = require("./PartService");
const PartSchema = require("./PartSchema");

class PartController extends ApiController {
  searchable = [
    "partNumber",
    "description",
    "hsCode",
    "interchangable",
    "countryOfOrigin",
  ];

  defaultOrder = "partNumber";

  defaultSort = "asc";

  include = ["Category", "Vendor"];

  constructor() {
    super(PartService, PartSchema);
  }
}

module.exports = PartController;
