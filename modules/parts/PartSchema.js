module.exports = {
  partNumber: { type: "string" },
  description: { type: "string", optional: true },
  hsCode: { type: "string" },
  interchangable: { type: "string" },
  countryOfOrigin: { type: "string" },
  duty: { type: "string" },
  freight: { type: "string" },
  // stock: { type: "number" },
  // price: { type: "number" },
  CategoryId: { type: "number" },
  VendorId: { type: "number" },
};
