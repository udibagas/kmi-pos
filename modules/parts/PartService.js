const { Part } = require("../../models");
const ApiService = require("../../services/ApiService");

class PartService extends ApiService {
  constructor() {
    super(Part);
  }
}

module.exports = new PartService();
