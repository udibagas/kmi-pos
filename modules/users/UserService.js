const { User } = require("../../models");
const ApiService = require("../../services/ApiService");

class UserService extends ApiService {
  constructor() {
    super(User);
  }

  async getOneByEmail(email) {
    const user = await User.scope("withPassword").findOne({ where: { email } });
    if (!user) throw { message: "Invalid email" };
    return user;
  }
}

module.exports = new UserService();
