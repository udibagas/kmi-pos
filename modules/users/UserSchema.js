module.exports = {
  name: { type: "string" },
  email: { type: "email" },
  password: { type: "string", min: 8, optional: true },
  status: { type: "boolean", optional: true },
};
