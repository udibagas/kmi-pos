const { compareSync } = require("bcryptjs");
const { ApiController, router } = require("../../controllers/ApiController");
const UserSchema = require("./UserSchema");
const UserService = require("./UserService");

class UserController extends ApiController {
  searchable = ["name", "email"];

  filterable = ["status", "role"];

  defaultOrder = "name";

  defaultSort = "asc";

  routes = ["all", "resetPassword", "index", "show", "create", "update", "destroy"];

  constructor() {
    super(UserService, UserSchema);
  }

  resetPassword(path) {
    return router.put(`${path}/resetPassword`, async (req, res) => {
      const { oldPassword, newPassword, confirmPassword } = req.body 

      if (newPassword !== confirmPassword) {
        return res.status(400).json({ 
          status: 400,
          name: "ValidationError", 
          message: "Validation Error",
          errors: { confirmPassword: ["Password doesn't match!"] }
        })
      }

      if (!compareSync(oldPassword, req.user.password)) {
        return res.status(400).json({ 
          status: 400,
          name: "ValidationError", 
          message: "Validation Error",
          errors: { oldPassword: ["Wrong password"] }
        })
      }

      try {
        await UserService.updateById({ password: newPassword }, req.user.id)
        res.json({message: "Password successfully updated"})
      } catch (error) {
        res.status(error.status || 500).json(error)
      }
    });
  }
}

module.exports = UserController;
