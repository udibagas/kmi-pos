class ApiService {
  constructor(model) {
    this.model = model;
  }

  sequelizeError = [
    "SequelizeValidationError",
    "SequelizeUniqueConstraintError",
  ];

  async getAll(options = {}) {
    return await this.model.findAll(options);
  }

  async getOneById(id, options = {}) {
    id = +id;
    if (isNaN(id)) throw { message: "Invalid ID", status: 500 };
    const data = await this.model.findByPk(id, options);
    if (!data) throw { message: "Data not found", status: 404 };
    return data;
  }

  async create(data) {
    try {
      return await this.model.create(data);
    } catch (error) {
      const { name, message } = error;

      if (this.sequelizeError.includes(name)) {
        const errors = this.parseError(error);
        throw { status: 400, name: "ValidationError", errors };
      }

      throw { status: 500, name, message };
    }
  }

  async updateById(data, id) {
    try {
      let instance = await this.getOneById(id);

      try {
        return await instance.update(data);
      } catch (error) {
        const { name, message } = error;

        if (this.sequelizeError.includes(name)) {
          const errors = this.parseError(error);
          throw { status: 400, name: "ValidationError", errors };
        }

        throw { status: 500, name, message };
      }
    } catch (error) {
      throw error;
    }
  }

  async destroyById(id) {
    try {
      let instance = await this.getOneById(id);
      try {
        return await instance.destroy();
      } catch (error) {
        throw error;
      }
    } catch (error) {
      throw error;
    }
  }

  async paginate(options = {}, page = 1, limit = 10) {
    const offset = (page - 1) * limit;
    limit = +limit;

    const data = await this.model.findAndCountAll({
      ...options,
      limit,
      offset,
    });

    const { count: total, rows } = data;

    return {
      total,
      page: +page,
      pageSize: limit,
      from: offset + 1,
      to: offset + limit >= total ? total : offset + limit,
      rows,
    };
  }

  parseError(error) {
    const errors = {};
    error.errors.forEach((e) => {
      typeof errors[e.path] === "undefined"
        ? (errors[e.path] = [e.message])
        : errors[e.path].push(e.message);
    });

    return errors;
  }
}

module.exports = ApiService;
