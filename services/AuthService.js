const UserService = require("../modules/users/UserService");
const { compareSync } = require("bcryptjs");
const { sign } = require("jsonwebtoken");

module.exports = class AuthService {
  static async login(email, password) {
    try {
      const user = await UserService.getOneByEmail(email);
      if (!user) throw { message: "Invalid email" };
      const validPassword = compareSync(password, user.password);
      if (!validPassword) throw { message: "Invalid password" };

      const token = sign({ user_id: user.id, email }, process.env.TOKEN_KEY, {
        expiresIn: "24h",
      });

      return { user, token };
    } catch (error) {
      throw error;
    }
  }
};
