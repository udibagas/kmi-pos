"use strict";

require("dotenv").config();
const nodemailer = require("nodemailer");

class MailerService {
  transporter;

  constructor() {
    this.createTransporter();
  }

  createTransporter() {
    const {
      SMTP_HOST: host,
      SMTP_PORT: port,
      SMTP_SECURE: secure,
      SMTP_USER: user,
      SMTP_PASS: pass,
    } = process.env;

    this.transporter = nodemailer.createTransport({
      host,
      port,
      secure: secure == "true",
      auth: { user, pass },
      requireTLS: true,
      tls: {
        minVersion: "TLSv1",
        rejectUnauthorized: false,
      },
    });
  }

  verifyTransporter(cb) {
    this.transporter.verify((error, success) => {
      if (error) console.log(error);
      else console.log("Ready to send messages");
      if (typeof cb === "function") cb(error, success);
    });
  }

  send(message, cb) {
    const { SMTP_FROM: from } = process.env;
    message = { from, ...message };

    this.transporter.sendMail(message, (err, info) => {
      if (err) console.log(err);
      else console.log("Success", info);

      if (typeof cb === "function") cb(err, info);
    });
  }
}

const mailer = new MailerService();
// mailer.verifyTransporter();
mailer.send({
  to: "udibagas@wide.co.id",
  subject: "Test email pakai nodemailer",
  text: "Ini test email pakai nodemailer",
  html: "<p>Ini test email pakai nodemailer</p>",
});

module.exports = mailer;
