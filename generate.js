"use strict";

const fs = require("fs");
const { pascalCase } = require("change-case");

const { singular } = require("pluralize");

const [command, ...params] = process.argv.slice(2);

if (command == "module") {
  const name = params[0];
  const singularName = singular(name);
  const serviceName = pascalCase(`${singularName} service`);
  const controllerName = pascalCase(`${singularName} controller`);
  const schemaName = pascalCase(`${singularName} schema`);

  const serviceTemplate = fs.readFileSync("./templates/Service.txt", "utf-8");
  const controllerTemplate = fs.readFileSync(
    "./templates/Controller.txt",
    "utf-8"
  );
  const schemaTemplate = fs.readFileSync("./templates/Schema.txt", "utf-8");

  fs.mkdirSync(`./modules/${name}`);

  fs.writeFile(
    `./modules/${name}/${serviceName}.js`,
    serviceTemplate.replaceAll("[name]", pascalCase(singularName)),
    (err) => {
      if (err) console.log("Failed to create service", err);
      else console.log(`${serviceName} has been created!`);
    }
  );

  fs.writeFile(
    `./modules/${name}/${controllerName}.js`,
    controllerTemplate.replaceAll("[name]", pascalCase(singularName)),
    (err) => {
      if (err) console.log("Failed to create service", err);
      else console.log(`${controllerName} has been created!`);
    }
  );

  fs.writeFile(
    `./modules/${name}/${schemaName}.js`,
    schemaTemplate.replaceAll("[name]", pascalCase(singular(name))),
    (err) => {
      if (err) console.log("Failed to create service", err);
      else console.log(`${schemaName} has been created!`);
    }
  );
}
