"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.addColumn("PurchaseApprovals", "UserId", {
      type: Sequelize.INTEGER,
      references: {
        model: "Users",
      },
    });

    await queryInterface.addColumn("PurchaseApprovals", "note", {
      type: Sequelize.TEXT,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    await queryInterface.removeColumn("PurchaseApprovals", "UserId");
    await queryInterface.removeColumn("PurchaseApprovals", "note");
  },
};
