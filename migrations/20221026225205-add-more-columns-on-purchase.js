'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.addColumn('Purchases', 'vendorAttention', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'destination', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'shippingMode', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'deliveryCondition', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'comment', { type: Sequelize.TEXT })
    await queryInterface.addColumn('Purchases', 'orderType', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'partialShipment', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'paymentType', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'vendorTerm', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'packingCondition', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'customerOrderNumber', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'currency', { type: Sequelize.STRING })
    await queryInterface.addColumn('Purchases', 'note', { type: Sequelize.TEXT })
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    const columns = [
      'vendorAttention',
      'destination',
      'shippingMode',
      'deliveryCondition',
      'comment',
      'orderType',
      'partialShipment',
      'paymentType',
      'vendorTerm',
      'packingCondition',
      'customerOrderNumber',
      'currency',
      'note'
    ]

    for (let col of columns) {
      await queryInterface.removeColumn('Purchases', col)
    }
  }
};
