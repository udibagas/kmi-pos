"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.addColumn("Purchases", "subTotal", {
      type: Sequelize.BIGINT,
    });
    await queryInterface.addColumn("Purchases", "shippingCost", {
      type: Sequelize.BIGINT,
    });
    await queryInterface.addColumn("Purchases", "tax", {
      type: Sequelize.BIGINT,
    });
    await queryInterface.addColumn("Purchases", "discount", {
      type: Sequelize.BIGINT,
    });
    await queryInterface.addColumn("Purchases", "totalAmountBalance", {
      type: Sequelize.BIGINT,
    });
    await queryInterface.addColumn("Purchases", "totalAmountBalanceSaid", {
      type: Sequelize.TEXT,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    const columns = [
      "subTotal",
      "shippingCost",
      "tax",
      "discount",
      "totalAmountBalance",
      "totalAmountBalanceSaid",
    ];

    for (let c of columns) {
      await queryInterface.removeColumn("Purchases", c);
    }
  },
};
