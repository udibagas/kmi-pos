"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.addColumn("Purchases", "vendorPhone", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn("Purchases", "vendorFax", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn("Purchases", "vendorAddress", {
      type: Sequelize.TEXT,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    await queryInterface.removeColumn("Purchases", "vendorPhone");
    await queryInterface.removeColumn("Purchases", "vendorFax");
    await queryInterface.removeColumn("Purchases", "vendorAddress");
  },
};
