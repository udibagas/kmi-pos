"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Machines", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      machineModel: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      machineCode: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      machineSerialNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      operatingWeight: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      bucketCapacity: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      engineModel: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      engineRatedPower: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      engineDisplacement: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      engineSerialNumber: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      stock: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      price: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      CategoryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Machines");
  },
};
