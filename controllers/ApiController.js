const { Op } = require("sequelize");
const validate = require("../middlewares/ValidatorMiddleware");
const router = require("express").Router();

class ApiController {
  searchable = [];

  filterable = [];

  defaultOrder = "updatedAt";

  defaultSort = "desc";

  include = [];

  routes = ["all", "index", "show", "create", "update", "destroy"];

  constructor(service, schema) {
    this.service = service;
    this.schema = schema;
  }

  index(path) {
    return router.get(path, async (req, res) => {
      const { search, page, pageSize, order, sort } = req.query;
      const options = { where: {} };

      if (search && this.searchable.length > 0) {
        options.where[Op.or] = {};

        this.searchable.forEach((col) => {
          options.where[Op.or][col] = { [Op.like]: `%${search}%` };
        });
      }

      this.filterable.forEach((col) => {
        if (req.query[col] !== undefined) {
          options.where[col] = {
            [Op.in]: req.query[col],
          };
        }
      });

      options.include = this.include;

      options.order = [[order || this.defaultOrder, sort || this.defaultSort]];
      const data = await this.service.paginate(options, page, pageSize);
      res.json(data);
    });
  }

  show(path) {
    return router.get(`${path}/:id`, async (req, res) => {
      try {
        const data = await this.service.getOneById(req.params.id, {
          include: this.include,
        });
        res.json(data);
      } catch (error) {
        res.status(error.status || 500).json(error);
      }
    });
  }

  create(path) {
    return router.post(path, validate(this.schema), async (req, res) => {
      try {
        const data = await this.service.create(req.body);
        res
          .status(201)
          .json({ status: 201, data, message: "Data ahs been saved" });
      } catch (error) {
        res.status(error.status || 500).json(error);
      }
    });
  }

  update(path) {
    return router.put(
      `${path}/:id`,
      validate(this.schema),
      async (req, res) => {
        try {
          const data = await this.service.updateById(req.body, req.params.id);
          res.send({ status: 200, data, message: "Data has been updated" });
        } catch (error) {
          res.status(error.status || 500).json(error);
        }
      }
    );
  }

  destroy(path) {
    return router.delete(`${path}/:id`, async (req, res) => {
      try {
        const data = await this.service.destroyById(req.params.id);
        res.send({ status: 200, data, message: "Data has been destroyed" });
      } catch (error) {
        res.status(error.status || 500).json(error);
      }
    });
  }

  all(path) {
    return router.get(`${path}/all`, async (req, res) => {
      const { order, sort } = req.query;

      try {
        const data = await this.service.getAll({
          order: [[order || this.defaultOrder, sort || this.defaultSort]],
        });

        res.json(data);
      } catch (error) {
        res.status(error.status || 500).json(error);
      }
    });
  }
}

module.exports = { ApiController, router };
