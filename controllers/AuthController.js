const AuthService = require("../services/AuthService");

exports.login = async (req, res) => {
  const { email, password } = req.body;
  try {
    const credential = await AuthService.login(email, password);
    res.json(credential);
  } catch (error) {
    res.status(404).json(error)
  }
};
